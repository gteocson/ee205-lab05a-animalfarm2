///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nunu.cpp
/// @version 1.0
///
/// Exports data about all nunu fish
///
/// @author Geoffrey Teocson <gteocson@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   26_02_2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "nunu.hpp"

using namespace std;

namespace animalfarm {

Nunu::Nunu( bool isNative, enum Color newColor, enum Gender newGender ) {
   gender = newGender;
   species = "Fistularia chinensis";
   scaleColor = newColor;
   favoriteTemp = 80.6;
   boolnative = isNative;
}

const string Nunu::speak(){
   return string( "Bubble Bubble" );
}

void Nunu::printInfo() {
   cout << "Nunu" << endl;
   cout << "   Is native = [" <<  boolalpha << boolnative <<  "]" << endl;
   Fish::printInfo();
}

}
